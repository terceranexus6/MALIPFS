<!DOCTYPE html>
<html>
<title>IPFS malware</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", Arial, Helvetica, sans-serif}
.myLink {display: none}
</style>
<body class="w3-light-grey">


<!-- Navigation Bar -->
<div class="w3-bar w3-white w3-border-bottom w3-xlarge">
  <a href="https://gitlab.com/terceranexus6">
          <span class="glyphicon glyphicon-sunglasses"></span>
  </a>
</div>

<!-- Header -->
<header class="w3-display-container w3-content w3-hide-small" style="max-width:1500px">
  <img class="w3-image" src="https://images.unsplash.com/photo-1557250643-c375a77aee64?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80" alt="London" width="1500" height="700">
  <div class="w3-display-middle" style="width:65%">
    <div class="w3-bar w3-black">
      <button class="w3-bar-item w3-button tablink" onclick="openLink(event, 'info');"><span class="glyphicon glyphicon-screenshot"></span> BASIC</button>
      <button class="w3-bar-item w3-button tablink" onclick="openLink(event, 'descripcion');"><span class="glyphicon glyphicon-pencil"></span> DESCRIPTION</button>
      <button class="w3-bar-item w3-button tablink" onclick="openLink(event, 'encrypt');"><span class="glyphicon glyphicon-alert"></span> ENCRYPT</button>
    </div>

    <!-- Tabs -->
    <div id="info" class="w3-container w3-white w3-padding-16 myLink">
      <h3>Basic information</h3>
      <div class="w3-row-padding" style="margin:0 -16px;">
	<form action="index.php" method="get">
        <div class="w3-half">
          <label>Name</label>
          <input class="w3-input w3-border" type="text" name="name" placeholder="Common name">
        </div>
        <div class="w3-half">
          <label>Hash</label>
          <input class="w3-input w3-border" type="text" name="hash" placeholder="SHA256">
    
	</div>
	<input type="submit" value="Save information">    
      </div>
            
<?php

$nam = $_GET["name"];
$ha = $_GET["hash"];

exec("touch data");
exec("echo $nam >> data");
exec("echo $ha >> data");

?>

    </div>

    <div id="descripcion" class="w3-container w3-white w3-padding-16 myLink">
      <h3>Add a brief description</h3>
      <form action="index.php" method="get">
      <input class="w3-input w3-border" type="text" name="des" placeholder="This malware does...">
      <input type="submit" value="Save information">    
    </div>
    
<?php

$desc = $_GET["des"];
exec("echo $desc >> data");

?>

    <div id="encrypt" class="w3-container w3-white w3-padding-16 myLink">
      
      <h3>Encrypt?</h3>
      <p>Do you want to encrypt the file?</p>
      <p>TEMPORARY OUT OF SERVICE!!</p>
      <p>It's not recommended if you want other people to have the information.</p>
      <form action="index.php" method="get">
      <input class="w3-input w3-border" type="text" name="passw" placeholder="Password...">    
      <input type="submit" value="Encrypt">
    </div>
  </div>
</header>

<?php

if ($_GET["passw"]) {
    $pass = $_GET["passw"];
    exec("sudo ./scripts/encrypt_file.sh data $pass");
    exec("rm data");
}
?>

 <div class="w3-container">
    <div class="w3-panel w3-padding-16 w3-black w3-opacity w3-card w3-hover-opacity-off">
      <h2>Send to IPFS!</h2>
      <p>Remember you will be able to get the hash back, but you wont's be able to delete it.</p>
      <a href="?send=true"><button type="button" class="w3-button w3-red w3-margin-top">SEND</button></a>
    </div>
  </div>

<?php
if ($_GET['send']) {
  # This code will run if ?run=true is set.
  exec("./scripts/ipfs_launcher.sh");
}
?>

<!-- Footer -->
<footer class="w3-container w3-center w3-opacity w3-margin-bottom">
 
  <p>Powered by <a href="https://inversealien.space" target="_blank" class="w3-hover-text-green">alien</a></p>
</footer>

<script>
// Tabs
function openLink(evt, linkName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("myLink");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
  }
  document.getElementById(linkName).style.display = "block";
  evt.currentTarget.className += " w3-red";
}

// Click on the first tablink on load
document.getElementsByClassName("tablink")[0].click();
</script>

</body>
</html>
