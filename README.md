# MALIPFS

IPFS launcher in php for malware IoC

![](images/test_php.gif)

## STEPS

**1 - Install IPFS and PHP**

For installing IPFS in debian/ubuntu is better to use snap, as described in the official [documentation](https://docs.ipfs.io/install/).

```
snap install ipfs
```

For installing php it's nice to use apt:

```
sudo apt -y install php7.4
```

**2 - Init IPFS**

As the documentation describes, this is needed for starting using ipfs

```
ipfs init
```

**3 - Launch in the localhost**

Launching using php:

```
php -S 127.0.0.1:8000
```

Visit [127.0.0.1:8000](127.0.0.1:8000), fill up name, hash and description in the different tabs (use save information button) and when you are done click in the button below that says "SEND" in the section "Send to IPFS". 

You can later check the hashes in IPFS_INFO file that the script creates.

```
cat IPFS_INFO
```
