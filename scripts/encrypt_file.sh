#!/bin/bash


echo "encrypting with a given password..."
openssl enc -aes-256-cbc -salt -in $1 -out "$1".enc -k $2

# for decrypt use
# openssl enc -aes-256-cbc -d -in file.txt.enc -out file.txt -k PASS
